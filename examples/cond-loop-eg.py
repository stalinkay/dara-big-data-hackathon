y = 3
if y == 4:
    print("Variable y is equal to 4")
else:
    print("In fact the variable is not equal to 4")

z = 2

if y > 2 and z in [3,4,5,6,7]:
    print("things are going to happen to you said the grand bubba!")
elif y == 2 or not (z in [2,3,5,6]):
    print("It's the one in the middle...")
else:
    print("oh really? just at the tail end...")


for val in [1,2,3,4,5,6,7,8]:
    print(val)

count = 0

while (count <10):
    print("current value of count is %d" % count)
    count +=1

print("testing another version...")

second_count = 0
while second_count < 20:
    if second_count % 2 == 0:
        print("second count is %d" % second_count)
    second_count += 1
