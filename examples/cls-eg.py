class Animal:

    def __init__(self, name):
        self.name = name

    def shout(self):
        print("Hello world! I am a %s..."%(self.name))

cat = Animal("green cat")
cat.shout()
