# DARA Big Data hackathon

<p align="center"><img width=50% style="margin-top: 0px" src="media/nust.png"><img width=25% src="media/daralogo.png"></p>

## Introduction

<!-- ## Data

The hackathon data can be found at the [Windhoek Hackathon Data](https://github.com/darabigdata/WindhoekHack)

----- -->

This repository provides a collection of python resources to build a machine learning model. Throughout this tutorial we will use [python3](https://www.python.org/downloads/release/python-373/)

## Install Python3

The following [link](https://realpython.com/installing-python/) provides installation steps on various operating systems, including Mac, Windows and Linux.

## Quick Introduction to Python

Most of the examples in this tutorial are extracted from [Learn Python](https://www.learnpython.org/). Here, we highlight some key features of the language.

### Variables and Types

The python programming language comes with a variety of in-built types, including **Boolean**, **Numeric** (int, float), **String**, **Dictionary**, **List**, **Set**, etc. [Python Data Types](https://docs.python.org/3/library/datatypes.html) provides a list of in-built data types in python3. New types can also be created in python through classes.

Because python is a dynamically typed language, there is no need to declare a variable or its type. Through a simple assignment, a variable is identified and its type inferred.

Examples
```python
y = 3
name = "Tonny"
```

### Conditions and Loops

Conditions can be defined in python using the *if*, *elif* and *else* keywords. The expression in the condition can use comparison functions between variables or literals.
Moreover, *for* and *while* loops can be used for iterations.

Examples
- conditions
- for loop
- while loop

```python
y = 3
if y == 4:
    print("Variable y is equal to 4")
else:
    print("In fact the variable is not equal to 4")

for val in [1,2,3,4,5,6,7,8]:
    print(val)

for val1 in range(3,8):
    print("current value is %d"%(val))

count = 0

while (count <10):
    print("current value of count is %d" % count)
    count +=1
```

### Functions

The keyword *def* is used to define a function. It may take arguments or not. It may also return a value.

```python
def sum_them_up(a,b):
    return a + b

print("the sum of %d and %d is %d"%(10, 4, sum_them_up(10,4)))
```

### Classes and Objects

Just like most object-oriented languages, classes represent a template and objects are instantiation of the template. Note that the *class* keyword is used to define a class.
Attributes inside a class are defined just as typical variables. Usually, there is no need to define a constructor for a class as a default one is provided for the class. However, it instantiates the resulting object as an empty object. To instantiate an object with custom initialisation of its attributes, the *\_\_init\_\_* function is used for that purpose.

```python
class Animal:

    def __init__(self, name):
        self.name = name

    def shout(self):
        print("Hello world! I am a %s..."%(self.name))

cat = Animal("green cat")
cat.shout()
```

### I/O

There are usually several levels of input/output in most languages:
- standard input or output
- file-level input or output

Standard I/O
1. The *input* function is used to get information from the user .
2. The *print* function is used to display on the standard output.

File I/O
1. The *open* function provides access to a file
2. The *close* function closes access to a file
3. The *read* and *write* functions get information from a file or add to it

```python
num = input("please enter a number between 3 and 10: ")
print("the entered value is %d" %(int(num)))

fobj = open("eg.txt", "rb+")
fobj.write("trying to show how to access and write to a file\n in python")
fobj.close()
```

## Machine Learning with Python

### Quick Intro

Machine learning can be thought of a sub-field of **artificial intelligence** where the focus is to build models that learn from and make predictions on data. Three essential categories of algorithms exist:
1. supervised algorithms, where the learning process builds on labelled data
2. unsupervised algorithms, where no prior label is used leaving the algorithms to find things by themselves
3. semi-supervised algorithms, where labelled data is mixed with unlabelled data
4. reinforcement algorithms, which try to achieve a goal with a reward mechanism as a payoff for each task

From these categories, various problems can be solved, including regression, classification, clustering, decision tree, etc. In this tutorial, we shall focus on **artificial neural networks** and **deep learning** models.

A short introduction to machine and deep learning can be found [here](https://www.math.univ-toulouse.fr/~besse/Wikistat/pdf/st-m-hdstat-rnn-deep-learning.pdf). Goodfellow, Bengio and Courville wrote a [book](http://www.deeplearningbook.org/) on Deep learning. Materials from the Deep Learning Indaba 2019 are accessible [here](http://www.deeplearningindaba.com/practicals-2019.html). Anna Scaife's materials can be found [here](https://as595.github.io/classification/) and [here](https://as595.github.io/frdeepcnn/).

### More Tutorials

1. [Your First Machine Learning Project in Python Step-By-Step](https://machinelearningmastery.com/machine-learning-in-python-step-by-step/)
2. [Beginner’s Guide to Machine Learning with Python](https://towardsdatascience.com/beginners-guide-to-machine-learning-with-python-b9ff35bc9c51)
3. [Machine Learning Tutorial Part - 1 | Machine Learning Tutorial For Beginners Part - 1 | Simplilearn](https://www.youtube.com/watch?v=DWsJc1xnOZo)
4. [Machine Learning Tutorial Part - 2 | Machine Learning Tutorial For Beginners Part - 2 | Simplilearn](https://www.youtube.com/watch?v=_Wkx_447zBM)
5. [Python Machine Learning Tutorial #1 - Introduction](https://www.youtube.com/watch?v=ujTCoH21GlA)

### DARA Big Data is supported by:

<p align="center"><img width=20% src="media/Newton-Fund-Master-rgb.jpg", hspace="20"><img width=40% src="media/stfc_logo.png", hspace="20"><img width=20% src="media/dst_logo_crop.jpeg"></p>
<p align="center"><img width=40% src="media/image002.jpg", hspace="20"><img width=20% src="media/idia_logo.png", hspace="20"></p>
